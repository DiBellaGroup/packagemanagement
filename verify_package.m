function versionsMatch = verify_package(packageName, expectedVersion)
  import PackageManagement.*
  versionCommand = [packageName, '.version;'];
  try
    VERSION = eval(versionCommand);
    versionsMatch = check_major_version(VERSION, expectedVersion, packageName);
  catch
    message = ['-- The package ' packageName ...
               ' doesn''t appear to be on the path.'];
    disp(message)
    versionsMatch = 0;
  end

  % Verify package
  verifyCommand = [packageName, '.verify;'];
  eval(verifyCommand);

  % Error out if required version is missing!
  if versionsMatch
    ...
  else
    error(['Missing required package: ' packageName ' version ' ...
           expectedVersion])
  end
end
