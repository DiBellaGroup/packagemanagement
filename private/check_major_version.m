function versionsMatch = check_major_version(VERSION, expectedVersion, packageName)
  majorVersion = regexp(VERSION,'^\d*','match');
  majorVersion = majorVersion{1};
  versionsMatch = majorVersion == expectedVersion;

  if versionsMatch
    message = ['-- ' packageName ' is OK.'];
  else
    message = ['-- ERROR - ' packageName ' should be version ' ...
               expectedVersion  ' instead of ' majorVersion];
  end
  disp(message)
end
