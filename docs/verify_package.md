## Verify Package

This method take in the name of a package and the expected major version number of the package. It uses MATLAB's `eval` to ask find the package and check that it's major version number matches. If the eval fails then it produces a warning that the package can't be found. If it succeeds but the version number doesn't match it produces a suitable warning.
