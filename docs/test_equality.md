# Test Equality


This function checks to see if two things are equal. It also provides an error message if they are not, stopping tests. I hope in the future to grow this, and perhaps put it in its own package. There are definitely better ways to manage testing.
