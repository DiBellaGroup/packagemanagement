function verify_self(expectedVersion)
  currentVersion = PackageManagement.version;
  majorVersion = regexp(currentVersion,'^\d*','match');
  majorVersion = majorVersion{1};
  if expectedVersion == majorVersion
    % do nothing
  else
    % error out
    error(['Package Manager is version ' majorVersion 'expected ' expectedVersion])
  end
end
