# Package Management

## Introduction

This package adds functions and classes that are needed to manage packages such as testing and version control.

### Version

The VERSION.mat file contains a single variable `VERSION` which contains a string with the version number of this package. No modifications to this file should be made without updating this number according to [semantic versioning best practices](http://semver.org/).

### Tests

Right now there are no tests for this package. That's a bit precarious but it's a little tough to get meta in MATLAB so for now that'll have to wait I'm afraid.

### Docs

In the `docs/` directory some markdown files are provided explaining how to use each of the functions and classes this package provides.
