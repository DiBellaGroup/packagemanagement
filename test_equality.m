function test_equality(matrixA, matrixB)
% deprecated, use Tester class for forwards compatibility
  if isequal(matrixA, matrixB)
    % do nothing
  else
    message = 'Test Failure! Please see MATLAB output BELOW this line.';
    error(message)
  end
end
